class CreateImageFlags < ActiveRecord::Migration
  def change
    create_table :image_flags do |t|
      t.integer :image_id
      t.integer :user_id_

      t.timestamps
    end
  end
end
