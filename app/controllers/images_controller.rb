class ImagesController < ApplicationController
  before_filter :authenticate, :except => [:index, :votes, :recent]

  def index
    recent
  end
  def recent
    @images = Image.order("created_at DESC").page(params[:page])
    if current_user.nil?
      @logged = false
    end
    @category = :recent
    render :index
  end
  def new_comment
    @comment = Comment.new(params[:comment])
    @comment.save!
    @image = Image.find(@comment.image_id)
    respond_to do |format|
      format.html {redirect_to :action => :index, :notice => "Comment successful"}
      format.js 
    end
  end
  def votes
    @comment = Comment.new
    @category = :votes
    @images = Image.order("upscore + downscore DESC").page(params[:page])
    render :index
  end
  def upvote
    @image = Image.find_by_id(params[:id])
    @image.upvote
    Vote.create!( :user => current_user, :image => @image, :upvote => true)
    @image.reload
    respond_to do |format|  
      format.html { redirect_to images_path }  
      format.js
    end     
  end
  def comment_upvote
    comment = Comment.find_by_id(params[:id])
    @image = Image.find(comment.image_id)
    comment.upvote
    Vote.create!( :user => current_user, :comment => comment, :upvote => true)
    respond_to do |format|
      format.html {redirect_to images_path}
      format.js
    end
  end
  def comment_downvote
    comment = Comment.find_by_id(params[:id])
    @image = Image.find(comment.image_id)
    comment.downvote
    Vote.create!( :user => current_user, :comment => comment, :upvote => false)
    respond_to do |format|
      format.html {redirect_to images_path}
      format.js
    end
  end
  def flagPicture
    image = Image.find_by_id(params[:id])
    UserMailer.flagPicture(image, current_user).deliver
    redirect_to images_path
  end
  def downvote
    @image = Image.find_by_id(params[:id])
    @image.downvote
    Vote.create!( :user => current_user, :image => @image, :upvote => false)
    @image.reload
    respond_to do |format|
      format.html {redirect_to images_path}
      format.js
    end
  end
  def create
    @image = Image.new(params[:image])
    @image.user_id = current_user.id
    @image.upscore = 0
    @image.downscore = 0
    if @image.save
      redirect_to :action => :index, :notice => "Image Successfully Uploaded"
    else
      render "new"
    end

  end
    def delete_comment
    comment = Comment.find_by_id(params[:id])
    comment.comment_text = '[deleted]'
    if comment.save
      redirect_to :action => :index, :notice => "Comment successfully deleted"
    end
  end
  def delete
    image = Image.find_by_id(params[:id])
    if image.delete
      redirect_to :action => :index, :notice => "Image successfully deleted"
    end
  end
  def new
    @image = Image.new
  end

protected
  def authenticate
    if !current_user
      redirect_to :controller => :sessions, :action => :new, :notice => "Please login before uploading image"
    end
  end
end
