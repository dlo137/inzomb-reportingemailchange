class UsersController < ApplicationController
  def new
    @user = User.new
  end
  def index
    @users = User.all
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      session[:user_id] = @user.id
      redirect_to  "/images", :notice => "Thanks for signing up!"
    else
      render "new"
    end
  end
  
  def description
    @user = User.find_by_name(params[:username])
    @user.description = params[:text]
    
    if @user.save
      render :nothing => true, :status => 200
    else
      render :nothing => true, :status => 422
    end   
  end
  
  def liked_photos
    @user = User.find_by_name(params[:username])
    if @user
      @images = Image.joins(:votes).order("votes.created_at DESC").where(:votes => {:user_id => @user.id, :upvote=> true } ).page(params[:page]).all
      @category = :liked
      respond_to do |format|  
        format.html { render 'users/profiles' }  
        format.js do
          if @images.blank?
            render :nothing => true
          end
        end
      end
    else
      redirect_to :controller => :image, :action => :index, :notice => "Invalid User Profile"
    end
  end
  def uploaded_photos
    @user = User.find_by_name(params[:username])
    if @user
      @images = Image.where(:user_id => @user.id).order("created_at DESC").page(params[:page])
      @category = :uploaded
      respond_to do |format|  
        format.html { render 'users/profiles' }  
        format.js do
          if @images.blank?
            render :nothing => true
          end
        end
      end
    else
      redirect_to :controller => :image, :action => :index, :notice => "Invalid User Profile"
    end
  end
end
