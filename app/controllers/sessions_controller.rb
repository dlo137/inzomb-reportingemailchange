class SessionsController < ApplicationController
  def new
    
  end
  def create
    user = User.authenticate(params[:name],params[:password])
    if user
      session[:user_id] = user.id
      redirect_to "/images/index", :notice => "Logged in"
    else
      flash[:error] = "Invalid username or password"
      render "new", :notice => "Invalid username or password"
    end
  end
  
  def destroy
    session[:user_id] = nil
    redirect_to "/images/index", :notice => "Logged out"
  end
end

