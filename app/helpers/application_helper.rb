module ApplicationHelper
  def profile_link(user)
    link_to( user.name, "/users/liked_photos/#{user.name}" )
  end
end
