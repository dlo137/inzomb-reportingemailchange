class UserMailer < ActionMailer::Base
  default :from => "inzombiac.net@gmail.com"

  def flagPicture(image, current_user)
    @image = image
    @user = current_user
    mail(:to => "gmyameen@gmail.com", :subject=> "Warning! Image has been flagged by #{@user.name}"  )
  end
end