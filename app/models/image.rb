class Image < ActiveRecord::Base
  belongs_to :user
  has_many :votes, :foreign_key => :image_id
  has_many :comments, :foreign_key => :image_id

  accepts_nested_attributes_for :comments
  validates_attachment_presence :image
  validates_attachment_size :image, :in => 1..3000000
  validates_attachment_content_type :image,  :content_type => ['image/jpeg', 'image/png']

  paginates_per 60

  has_attached_file :image,
    :styles => { :thumb => "100x100#", :thumb2 => "200x200#", :large => "1000x2000>"},
    :storage => :s3,
      :s3_credentials => S3_CREDENTIALS,
      :path => ":attachment/:id/:style.:extension",
      :url => ":s3_alias_url",
      :s3_host_alias => 'img.inzomb.com'

  def vote_exists(user)
    upvote_exists(user) || downvote_exists
  end
  def upvote_exists(user)
    return false if user.nil?
    self.votes.exists?(:user_id => user.id, :upvote => true)
  end

  def downvote_exists(user)
    return false if user.nil?
    self.votes.exists?(:user_id => user.id, :upvote => false)
  end

  def upvote
    self.upscore += 1
    self.save
  end
  def downvote
    self.downscore -= 1
    self.save
  end
  def score
    self.upscore
  end
  def self.ten_most_recent
    return order("created_at desc").limit(10)
  end
  def self.ten_most_votes
    return order("upscore desc").limit(10)
  end
  def recent_comments
    return self.comments.order("created_at desc").limit(5)
  end
end