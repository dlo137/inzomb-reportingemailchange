class ImageFlag < ActiveRecord::Base
  belongs_to :images, :foreign_key => :image_id
  belongs_to :user, :foreign_key => :user_id
end
