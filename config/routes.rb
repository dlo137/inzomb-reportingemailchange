Inzomb::Application.routes.draw do

  get "beta/index"

  post "images/new_comment" => "images#new_comment"

  post "beta/validate"


  get "images/index"
  post "images/new" => "images#create"
  get "images/upvote" => "images#upvote"
  get "images/downvote" => "images#downvote"
  get "images/comment_upvote" => "images#comment_upvote"
  get "images/comment_downvote" => "images#comment_downvote"
  get "images/flagPicture" => "images#flagPicture"
  get "images/votes" => "images#votes"
  get "images/recent" => "images#recent"
  get "images/new"
  get "images/delete" => "images#delete"
  get "sessions/new"
  post "users/description" => "users#description"

  get "users/new"
  get "users/liked_photos/:username" => "users#liked_photos"
  get "users/uploaded_photos/:username" => "users#uploaded_photos"

  get "upload" => "images#new", :as => "upload"

  get "images" => "images#index", :as => "images"
  get "logout" => "sessions#destroy", :as => "logout"
  get "login" => "sessions#new", :as => "login"
  get "signup" => "users#new", :as => "signup"

  root :to => "images#index"
  resources :users
  resources :sessions
  resources :images

  resources :beta

  #get "users/forgot"

  #get "users/signup"

  #get "users/login"

  #get "users/index"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/indexex.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
